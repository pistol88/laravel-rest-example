## Laravel App Example

Just an example of a REST API

### Install and running:

- git clone
- composer install
- php artisan jwt:secret
- php artisan migrate
- php artisan db:seed --class=CategoriesSeeder
- php artisan db:seed --class=TasksSeeder
- php artisan serve

to CRON:

````
* * * * php /path-to-your-project/artisan schedule:run >> /dev/null 2>&1
````
