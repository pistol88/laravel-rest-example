<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'api', 'namespace' => 'App\Http\Controllers\Api\V1', 'as' => 'api.', 'prefix' => 'v1'], function () {
    Route::post('auth/login', 'AuthController@login')->name('auth.login');
    Route::post('auth/registration', 'AuthController@registration')->name('auth.registration');
});

Route::group(['middleware' => ['api', 'jwt.verify'], 'namespace' => 'App\Http\Controllers\Api\V1', 'as' => 'api.', 'prefix' => 'v1'], function () {
    Route::get('tasks/index', 'TasksController@index')->name('tasks.index');
    Route::put('tasks/replace', 'TasksController@replace')->name('tasks.replace');
    Route::put('tasks/done', 'TasksController@done')->name('tasks.done');
});
