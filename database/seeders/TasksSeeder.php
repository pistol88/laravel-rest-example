<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Task;
use Illuminate\Database\Seeder;
use Faker\Factory;

class TasksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $categoryIds = Category::all()->pluck('id')->toArray();

        for ($i = 0; $i <= 1000; $i++) {
            (new Task())->fill([
                'name' => $faker->text(50),
                'text' => $faker->text(),
                'category_id' => $categoryIds[array_rand($categoryIds)]])
                ->save();
        }
    }
}
