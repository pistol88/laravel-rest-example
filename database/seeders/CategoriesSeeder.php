<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ['Fundamentals', 'String',  'Algorithms', 'Mathematic', 'Performance', 'Booleans', 'Functions'];

        foreach ($categories as $categoryName) {
            (new Category())->fill(['name' => $categoryName])->save();
        }
    }
}
