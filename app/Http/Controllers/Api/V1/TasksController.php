<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\TaskDone;
use App\Http\Requests\TaskReplace;
use App\Http\Resources\TaskCollection;
use App\Http\Resources\Task as TaskResource;
use App\Libs\TaskManager;
use App\Models\TaskUser;
use Illuminate\Routing\Controller as BaseController;
use JWTAuth;

class TasksController extends BaseController
{
    public function index()
    {
        $user = auth()->user();

        $items = $user->tasks()->whereRaw('Date(task_user.created_at) = CURDATE()')->get();

        return new TaskCollection($items);
    }

    public function done(TaskDone $request)
    {
        $user = auth()->user();

        $task = TaskUser::where('user_id', $user->id)->where('task_id', $request->input('task_id'))->orderBy('id', 'DESC')->firstOrFail();
        $task->status = TaskUser::STATUS_DONE;
        $task->save();

        return ['result' => 'ok'];
    }

    public function replace(TaskReplace $request)
    {
        $user = auth()->user();

        $taskManager = new TaskManager($user);
        $newTask = $taskManager->replace($request->input('task_id'));

        return new TaskResource($newTask);
    }
}
