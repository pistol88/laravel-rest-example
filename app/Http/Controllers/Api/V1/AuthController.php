<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegistrationRequest;
use App\Models\User;
use Illuminate\Routing\Controller as BaseController;
use JWTAuth;

class AuthController extends BaseController
{
    public function login(LoginRequest $request)
    {
        $credentials = $request->validated();

        if (!$token = JWTAuth::attempt($credentials)) {
            return response()->json(['error' => 'Wrong credentials'], 403);
        }

        return ['token' => $token];
    }

    public function registration(RegistrationRequest $request)
    {
        $userData = $request->validated();

        $user = new User();
        $user->fill($userData);
        $user->save();

        if (!$token = JWTAuth::attempt($userData)) {
            return response()->json(['error' => 'Wrong credentials'], 403);
        }

        return ['token' => $token];
    }
}
