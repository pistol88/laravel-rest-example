<?php

namespace App\Http\Resources;

use App\Models\TaskUser;
use Illuminate\Http\Resources\Json\JsonResource;

class Task extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'status' => $this->todaysUserDigest()->status,
            'category' => (new Category($this->resource->category))->toArray($request),
            'name' => $this->name,
            'text' => $this->text,
        ];
    }

    private function todaysUserDigest()
    {
        $user = auth()->user();

        return TaskUser::where('task_id', $this->id)->where('user_id', $user->id)->orderBy('id', 'DESC')->firstOrFail();
    }
}
