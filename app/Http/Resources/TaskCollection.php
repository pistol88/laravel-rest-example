<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class TaskCollection extends ResourceCollection
{
    public $collects = 'App\Http\Resources\Task';

    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
