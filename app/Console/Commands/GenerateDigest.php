<?php

namespace App\Console\Commands;

use App\Libs\TaskManager;
use App\Models\Task;
use App\Models\User;
use Illuminate\Console\Command;

class GenerateDigest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:digest';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Denerate a deily digest for all users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = User::all();

        foreach ($users as $user) {
            $taskManager = new TaskManager($user);
            $taskManager->generateDailyDigest();
        }
    }
}
