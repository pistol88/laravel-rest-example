<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name',
        'text',
        'category_id',
    ];

    public function tasks()
    {
        return $this->hasMany('App\Models\Task', 'category_id', 'id');
    }
}
