<?php

namespace App\Libs;

use App\Models\Category;
use App\Models\Task;
use App\Models\TaskUser;
use App\Models\User;

class TaskManager
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function generateDailyDigest()
    {
        $categories = Category::all();

        foreach ($categories as $category) {
            $task = $this->randomTask($category);

            if ($task) {
                $this->user->tasks()->attach($task->id);

                echo "task {$task->id} attached to user {$this->user->id} \n";
            } else {
                //alert: no one task left
            }
        }
    }

    public function replace($taskId) : Task
    {
        $this->user->tasks()->detach($taskId);

        $task = Task::whereId($taskId)->firstOrFail();
        $newTask = $this->randomTask($task->category);
        $this->user->tasks()->attach($newTask->id);

        return Task::whereId($newTask->id)->firstOrFail();
    }

    private function randomTask(Category $category)
    {
        $tableName = (new Task())->getTable();
        $tableName2 = (new TaskUser())->getTable();

        $task = \DB::select("
                SELECT * from `$tableName`
                WHERE category_id = ?
                    AND id NOT IN (SELECT task_id FROM $tableName2 WHERE user_id = ?)
                ORDER BY rand() LIMIT 1", [$category->id, $this->user->id]);

        return $task[0];
    }
}
